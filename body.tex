\section*{Abstract}

The security of the web is threatened by TLS proxies, which provide
substitute certificates to users and act as a man-in-the-middle
between two parties who believe they are communicating
securely. Because the same technology can be used for both benevolent
or malicious purposes, it is difficult to distinguish between the
two. We propose to create an open source tool for measuring the
prevalence of proxies and to secure it against spoofing by attackers.
We will then conduct several measurement campaigns to provide data on
the prevalence of TLS proxies on the Internet, including both web and
mobile platforms. Finally, we will propose protocols and mechanisms to
notify users and organizations when their connections are being
proxied for encrypted traffic, so we can increase awareness and enable
people to opt out of surveillance of encrypted traffic.  Significant
areas for collaboration with Comcast include 
understanding Comcast's use of TLS proxies, 
identifying Comcast
services that could deploy the open source measurement tool,
and receiving help from Comcast to measure TLS proxies affecting
their services and their users.

\section*{Research Problem}

% New outline to simplify and focus this section:
% \begin{enumerate}
% 
% \item Teach the reader what a TLS proxy is and how it works.
% 
% \item Identify the ways that a new trust root can be added to the user's root store
% 
% \item Central problem: Users cannot distinguish between benevolent and malicious uses
% 
% \item Emphasize that we don't have good measurements of how often this occurs
% 
% \end{enumerate}

Most secure communication on the Internet is provided by the Transport
Layer Security (TLS) protocol, which uses the Certificate Authority
(CA) system to authenticate servers to clients.  To provide proof of
its identity, a TLS server sends the client an X.509 certificate
that has been digitally signed by a CA. Clients are bootstrapped with a set of
trusted CAs in a {\em root store}, and if the certificate's signature
can be successfully decrypted using the public key of one of these
trusted CAs, it is considered valid.  Certificates may also be signed
by any number of intermediate CAs and are considered valid if the
chain of signatures can be traced back to a root CA that the client
trusts.  Once validated, the client uses the public key contained in
the server's certificate to initialize secure communication.

Though this mechanism may seem sufficient, it is possible to use
commodity hardware or software to act as a TLS proxy and decrypt the
traffic meant to be exchanged privately with a web site.  For example,
if a user installs a personal firewall, this product may add a new
certificate to the root store.  Now, when the user accesses a secure
site, the firewall issues a {\em substitute certificate} for the site
that is signed by the new root certificate. This causes the user to
establish an encrypted connection with the firewall, even though it
believes it is talking to the intended web site. The firewall in turn
contacts the original web site securely on behalf of the user, thus
acting as a Man-in-the-Middle (MitM) for the encrypted
conversation. This enables the firewall to monitor all traffic,
including encrypted traffic, for malware. Organizations use this same
technique to monitor traffic on internal networks using proxies that
intercept all web traffic.

The use of a TLS proxy depends on adding a new, trusted certificate
into the user's root store.  In addition to direct installation on a
user's machine, an organization may use a custom system image or an
enterprise PKI system. On mobile devices, a manufacturer may add
certificates directly to the device. For example, Nokia was recently
found to be using TLS proxies on mobile devices and has experienced a
public backlash against this being done without users' knowledge
\cite{nokia}.

However, a major problem with the use of TLS proxies is that malware
can use the same technique to intercept encrypted traffic from
unsuspecting users. For example, a malicious entity could steal a
user's credit card number, record all the sites a user visits, or
intercept encrypted chat sessions. To be successful, all the malware
needs is to add a new certificate to the user's root store when it is
unwittingly installed. Other possibilities include a security breach
at a certificate authority \cite{marlinspike2011ssl} or collusion with
a certificate authority. Since some CAs are owned by a government, it
is fairly easy for a government to use this method for surveillance
purposes.

%% the authentication system
%% has become increasingly brittle and abused. In particular, it exhibits
%% two alarming traits. \textit{(1) Clients trust too many CAs.} A scan
%% performed in 2010 \cite{eckersley2010observatory} revealed 1,482 CA
%% certificates trusted by Windows or Firefox. Each of these CAs must
%% follow best practices to be worthy of this trust, but the greatness of
%% their numbers makes it prohibitive for concerned citizens to audit
%% their behavior. \textit{(2) CAs can sign for any domain.} This
%% includes domains for which certificates are already issued.  Thus at
%% any time there may be many different ``valid'' certificates signed by
%% many different CAs for a single domain. A security breach of a single
%% CA can result in the signing of forged certificates for any domain.
%% Thus TLS authentication is only as strong as the weakest CA.

Currently, browsers and users have no method for distinguishing
between benevolent and malicious TLS proxies.  Even worse, in both
cases the user is completely oblivious to what is happening, because
the certificate chain is considered valid by the user's browser.  If
the user were to check the browser's security indicators, they would
indicate that his connection is legitimate and secure, instilling a
false sense of security. Moreover, this problem can potentially have
drastic long-term consequences.  Users and businesses are already
aware of the numerous attacks that can take place on the Internet, but
believe they are guaranteed a measure of protection by using TLS and
the CA system. However, if these vulnerabilities persist, users may
not be able to rely on encrypted communication, drastically eroding
their trust in the Internet as a platform for e-commerce and other
secure transactions.

Despite the availability of commercial TLS proxy software and the
ability of attackers to use this technique, we have little data on the
prevalence of TLS proxies.  To better understand the role that TLS
proxies play in today's Internet, our research focuses on detecting
and reporting the existence of proxies.  The use of TLS proxies is
controversial, with privacy advocates calling for mechanisms that
explicitly forbid them, while some organizations and users recognize
the benefits of inspecting encrypted traffic for security
purposes. Our work is the first step toward designing a new
certificate system.

%% Since we have submitted our paper (and the CMU paper is coming
%% out), we should be sure our discussion of related work and our
%% proposed work looks to the next steps such as mobile measurements,
%% security against tampering, and transparency

\section*{Preliminary Research}

Our preliminary research includes the design of a 
tool to measure the presence of a TLS proxy without any new software 
at the client. Our tool uses Flash to request
a certificate from a server we operate at BYU, then deliver the
received certificate back to us. Our server checks whether the
certificate they received was the same one we gave them, with a
mismatch revealing a proxy intercepting their traffic. Most existing
tools require installation of software by a client, whereas our tool
can be used by any client that supports Flash; we use Flash 9.0, which
has a penetration rate of 98.9\% in the desktop market. Because data
is collected by the server, our
tool allows sever administrators to deny access to
clients behind a TLS proxy if they feel this compromises the integrity
of an important financial transaction.

We used our tool to measure the prevalence of proxies by embedding the
 Flash application into an advertisement and serving it through a
Google AdWords campaign. With a small budget of \$5,000, we were able
to test 2.9 million connections to our server, finding 11,764 proxied
connections (0.41\%) spanning 142 countries. Examining the issuer
organization in the certificates (which we can only take at face
value) reveals that 54\% are from personal firewalls, and 16\% are
from corporate networks, indicating the majority are used for
benevolent purposes. 

We found several instances of negligent and malicious
behavior. Our analysis of one parental filter finds that it masks
forged certificates from an attacker, allowing the attacker to
easily perform a MitM attack against the firewall's users.  In
addition, we found three malware products affecting over 1,000
systems that install a new root certificate and act as a TLS proxy
to dynamically insert advertisements on secure sites.  We also found
evidence that spammers are using TLS proxies in their products, and
we found numerous other suspicious circumstances in
substitute certificates, such as a null Issuer Organization, 
falsified DigiCert signatures, and downgraded public key sizes.
 
To assess user attitudes toward TLS proxies, we conducted a survey of 1,261 users on their attitudes toward TLS
proxies, primarily from the US and India. The survey indicates that
users have a complicated relationship with proxies. They accept the
use of proxies for filtering and protecting against malware, but
strongly favor being informed when a proxy actively intercepts their
encrypted traffic. They overwhelmingly do not want law enforcement to
use TLS proxies, and strongly favor legislation requiring user consent
when proxies are used.

Our initial results are described in a paper that is currently under submission~\cite{oneill2014}.

\section*{Proposed Research}

We propose to extend our research in the following ways:

\begin{enumerate}

\item \textit{Open Source Measurement Tool:} We plan to develop an
  open source proxy measurement tool and make it available to service
  providers so that they can determine whether their customers are
  accessing the service behind a TLS proxy.  The result will provide
  evidence to help determine whether the proxy is legitimate or
  potentially malicious.  The customers will not have to install any
  new client-side software.  This code will be available within 6
  months of receiving funding and will use a BSD license. Dr. Zappala
  has contributed regularly to open source projects and our students
  have the resources to commit to contribute code to the project.

\item \textit{Improved Server-Side Measurement:} Additional measures are
  needed to ensure our tool cannot be circumvented by a proxy
  trying to avoid detection. 
  To prevent avoidance, we will place the main program in a
  loader that is encrypted using AES. Once decrypted by a minimal
  plaintext JavaScript library, the loader will be executed and the
  result sent back to our server using similar encryption.  In this
  way, the vulnerabilities in our tool are consolidated to the
  security of the key used for encryption. Protection of this key can
  be calibrated to taste by the website administrator. For our
  measurements, which have no pre-established relationship with
  users, the key can simply be sent to the client in JavaScript with
  some automated obfuscation to avoid real-time attacks.  If a server
  wishes to run our tool with a greater level of security, the key can
  be based on something known to the user and server but not the TLS
  proxy, such as a password security question. Another possibility is
  sending the key via text message.

\item \textit{Extended Measurement Campaign: } Our initial
  measurements used Google AdWords to detect proxies. We would like to
  work with high profile sites such as Comcast to gather additional
  data, covering areas where our current measurements do not reach.
  This will allow us to determine whether they are being specifically
  targeted by a proxy that might otherwise allow encrypted connections
  to pass unfiltered to our own server. To do this, we need a site's
  cooperation to grant our application access via their Flash socket
  policy file.  We are especially interested in collecting
  measurements from large numbers of clients for the Alexa Top 100
  sites, so we can assess user vulnerability for the sites they use
  the most.  We would also like to use additional advertising
  campaigns to target specific countries and determine if TLS proxies
  are more prevalent in certain areas, for example Asia or the Middle
  East. Finally, we want to have our server purposely issue a
  certificate that is forged, self-signed, or expired.  We can then
  determine whether proxies are masking invalid or unsafe
  certificates, exposing the user to servers with poor security
  practices without their knowledge.

\item \textit{Android Measurement Campaign:} Our measurement tool is
  effective only for desktops. To further broaden our measurements, we
  will develop an Android application to detect proxies from
  smartphones.  The mobile market is particularly intriguing because
  devices can be easily controlled by a carrier. To encourage adoption
  of our application, we will work with BYU marketing teams to create
  and advertise an application that acts as a useful security tool for
  safe web browsing.

\item \textit{Proxy Notification:} There are clearly cases where both
  individuals and organizations want to use benevolent TLS proxies to
  protect against malware. We want to develop protocols and mechanisms
  to notify all parties involved in a TLS exchange about any proxies
  on the connection, so that users and organizations can make informed
  decisions about whether the connection should proceed
  \cite{mcgrew2012internetdraft,loreto2014internetdraft,nir2012internetdraft}.
 
\end{enumerate} 

\section*{Comcast Collaboration}

Comcast support has the potential to significantly enhance the results
of this research in the following ways.  
\begin{enumerate}
\item Inform our research by discussing the ways that Comcast has found 
it necessary to operate TLS proxy technology to protect its networks from malware
and to safeguard customers.

\item Identify Comcast services that could use our proxy measurement technology 
to determine whether their customer's TLS connections are being proxied 
and gather data to determine the nature of those proxies.

\item Determine an appropriate Comcast service that could grant access to our application
in order to measure whether connections to Comcast are proxied during our 
Google AdWords campaign.

\item Determine the feasibility of getting help from Comcast to measure
connections from home users to the Alexa Top 100 sites, providing
more data for proxy detection. 
\end{enumerate}

A benefit of this collaboration is that we could help Comcast determine
which of their customers and clients are affected by malware
or are subject to MitM attacks.

\section*{Vision}

The proposed project is part of our long-term research vision to
develop a new certificate system that addresses problems with the
current CA trust model.  Our long-term goals for this
research include:

\begin{enumerate}

\item Eliminating the reliance on a trusted root store that can be
  modified by clients. This is a major weakness with the current
  system, since malware can modify the root store as well.

\item Incorporating transparency for legitimate TLS proxies, so that
  we can distinguish between attackers and hardware or software that is
  protecting users from malware.

\item Adding support for trust agility, so that corporations and users
  can decide who to trust. This addresses the current problem that
  users must trust all CAs equally, including those that have been
  compromised or those owned by foreign countries that may be hostile
  to their political views.

\item Designing a flexible framework that allows additional trust
  models to coexist with the current CA system, such as the use of
  notaries. This would provide the ability to migrate to a new certificate
  system while still using the existing system.

\item Focusing on a user-centric approach that ensures designs that
  inform users and help them make secure choices when using the Internet,
  without requiring a high degree of sophistication or knowledge.

\end{enumerate}


\section*{Related Work}

There has been considerable research effort to mitigate the
possibility of TLS MitM attacks.  Present solutions range from
strengthening the CA system to proposals of entirely new systems that
deprecate CAs. Two commonalities between all solutions are lack of
widespread deployment and lack of interoperability with benevolent TLS
proxies.

Several systems rely on multi-path probing to detect MitM attacks
(e.g., Perspectives~\cite{wendlandt2008perspectives},
Convergence~\cite{marlinspike2011ssl}, Crossbear~\cite{holz2012x}).
Another approach is to pin legitimate server certificates in the
browser in order to prevent the browser from accepting some other
certificate as legitimate (e.g., Certificate
pinning~\cite{evans2011certificate}, Trust Assertions for Certificate
Keys (TACK)~\cite{tack}).  Recent proposals rely on public log files
to help auditors detect rogue certificates (e.g., Certificate
transparency (CT)~\cite{rfc6962}, EFF sovereign keys (SK)
project~\cite{sovereignkeys}, Accountable Key Infrastructure
AKI)~\cite{kim2013accountable}).  None of these systems appear to
distinguish between malicious attackers and organizations providing
additional security to their users. Both cases will be flagged as an
error.

We recently discovered that another research group has also been measuring the prevalence of TLS proxies~\cite{huang2014analyzing}.
They deployed a similar Flash program to measure proxied connections to Facebook by deploying their tool on Facebook servers.
We each measured about 3 million connections, but we had a much higher rate of TLS proxies 
at 0.41\% with our Google AdWords campaign compared to 0.20\% connections to Facebook that were proxied.
The results corroborate each other in many respects. 
We discovered some malware not reported in the other study. 
We also identified a personal firewall product that masks forged certificates by creating a proxied connection to a server with an invalid certificate.


